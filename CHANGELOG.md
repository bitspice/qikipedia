### 0.1.3 - Feb 01, 2023
- Fixed an issue with the popup opening (thanks to @agurvets)

### 0.1.2 - April 04, 2019
- Added option for requiring Ctrl to be held down when highlighting, for popup to appear

### 0.1.1 - May 29, 2018
- Added option to disable automatically showing summary on highlight
- Added Ukrainian language

### 0.1.0 - May 25, 2018
- Initial release.