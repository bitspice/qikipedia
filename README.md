# Qikipedia

[![Download from Chrome Web Store](https://storage.googleapis.com/web-dev-uploads/image/WlD8wC6g8khYWPJUsQceQkhXSlv1/iNEddTyWiMfLSwFD6qGq.png "Download for Chrome")](https://chrome.google.com/webstore/detail/qikipedia/dljenobnbaealaglncgihenfkbkblefd)

Qikipedia is a Chrome browser extension that allows you to highlight text from any website, and shows you a summary of the corresponding Wikipedia article, if one exists.

![Qikipedia in Action](https://res.cloudinary.com/kahtaf/video/upload/h_600,w_1280,c_fill/v1527208553/qikipedia/qikipedia_demo.gif)

## Reporting issues

Found a bug? Please use our [issue tracker](https://gitlab.com/bitspice/qikipedia/issues), and remember to include the URL of where the bug was found.

## License

Qikipedia is released under Apache 2.0.